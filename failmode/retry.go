package failmode

import (
	"gitcode.com/common_go/producer/meta"
)

func init() {
	AddFailMode(meta.RETRY, InitRetry)
}

func InitRetry() (FailMode, error) {
	r := new(Retry)
	return r, nil
}

type Retry struct {
}

func (r *Retry) Do(fallback chan<- []byte, message []byte, data []byte, keyParams []interface{}) {
	fallback <- message
}
