package meta

const (
	Kafka    = "kafka"
	Rabbitmq = "rabbitmq"
	Rocketmq = "rocketmq"
	NSQ      = "nsq"
	Delay    = "delay"
)

const (
	RETRY   = "retry"
	DISCARD = "discard"
	SAVE    = "save"
)

const (
	REDIS   = "redis"
	FILE    = "file"
	LEVELDB = "leveldb"
)

const (
	RedisNilError = "redis: nil"
)
