package kafka

import (
	"bytes"
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"gitee.com/common_go/producer/common"
	"gitee.com/common_go/producer/failmode"
	"gitee.com/common_go/producer/internal"
	"gitee.com/common_go/producer/meta"

	"gitee.com/common_go/config"
	"gitee.com/common_go/logger"

	"github.com/Shopify/sarama"
	"github.com/spf13/cast"
)

type Kafka struct {
	input    chan []byte
	fallback chan<- []byte

	successCount int64
	errorCount   int64

	cfg      *sarama.Config
	producer sarama.AsyncProducer
	failMode failmode.FailMode

	exit        chan struct{}
	watchExit   chan struct{}
	successExit chan struct{}
	fallExit    chan struct{}
}

func Init(quit chan struct{}, fallBack chan<- []byte) common.MQ {
	cfg := sarama.NewConfig()
	partitioner := config.GetConfDefault("KafkaProxy", "KafkaPartitioner", "round")
	cfg.Version = sarama.V0_10_2_0
	cfg.Producer.Partitioner = sarama.NewRoundRobinPartitioner
	if partitioner == "hash" {
		cfg.Producer.Partitioner = sarama.NewHashPartitioner
	}
	if partitioner == "random" {
		cfg.Producer.Partitioner = sarama.NewRandomPartitioner
	}
	cfg.Producer.Timeout = cast.ToDuration(config.GetConfDefault("KafkaProxy", "KafkaProducerTimeout", "10")) * time.Second
	cfg.Producer.Flush.MaxMessages = 5000
	waitall := config.GetConf("KafkaProxy", "KafkaWaitAll")
	cfg.Producer.Return.Successes = true
	cfg.Producer.Return.Errors = true
	if config.GetConfDefault("KafkaProxy", "sasl", "false") == "true" {
		cfg.Net.SASL.Enable = true
		cfg.Net.SASL.User = config.GetConf("KafkaProxy", "user")
		cfg.Net.SASL.Password = config.GetConf("KafkaProxy", "password")
	}

	if waitall != "" {
		cfg.Producer.RequiredAcks = sarama.WaitForAll
	} else {
		cfg.Producer.RequiredAcks = sarama.WaitForLocal
	}
	compression := config.GetConf("KafkaProxy", "KafkaCompression")
	if compression != "" {
		cfg.Producer.Compression = sarama.CompressionSnappy
	}

	k := new(Kafka)
	k.cfg = cfg
	k.input = make(chan []byte, 0)
	k.fallback = fallBack
	k.exit = make(chan struct{}, 1)
	k.watchExit = make(chan struct{}, 1)
	k.successExit = make(chan struct{}, 1)
	k.fallExit = make(chan struct{}, 1)
	k.successCount = int64(0)
	k.errorCount = int64(0)

	k.initProducer()
	go k.success()
	go k.fall()
	go k.run(quit)

	return k
}

func (k *Kafka) Input() chan<- []byte {
	return k.input
}

func (k *Kafka) Close() {
	close(k.exit)
}

func (k *Kafka) SetFailMode() {
	var err error
	k.failMode, err = failmode.GetFailMode(meta.Kafka, config.GetConfDefault("KafkaProxy", "failMode", "discard"))
	if err != nil {
		logger.Wx(context.Background(), "SetFailMode", "Kafka SetFailMode error", "error", err.Error())
	}
}

func (k *Kafka) initProducer() {
	hosts := config.GetConfArr("KafkaProxy", "brokers")
	producer, err := sarama.NewAsyncProducer(hosts, k.cfg)
	if err != nil {
		logger.Wx(context.Background(), "initProducer", "ProducerInit", "error", err.Error())
	}
	k.producer = producer
}

func (k *Kafka) success() {
	defer meta.Recovery()
	for {
		select {
		case <-k.successExit:
			return
		case msg := <-k.producer.Successes():
			if msg != nil {
				atomic.AddInt64(&k.successCount, int64(1))

				byteMsg := msg.Metadata.([]byte)
				byteMsg = byteMsg[:0]
				internal.BytePool.Put(byteMsg)
			}
		}
	}
}

func (k *Kafka) fall() {
	defer meta.Recovery()

	ctx := context.Background()
	tag := "kafka.fall"

	for {
		select {
		case <-k.fallExit:
			return
		case err := <-k.producer.Errors():
			if err != nil {
				atomic.AddInt64(&k.errorCount, int64(1))
				items := bytes.SplitN(err.Msg.Metadata.([]byte), []byte(" "), 5)
				timestamp := string(items[0])
				loggerid, _ := err.Msg.Key.Encode()
				data, _ := err.Msg.Value.Encode()
				logFlag := cast.ToInt64(timestamp) % 10
				diff := time.Now().Unix() - cast.ToInt64(timestamp)/10
				if logFlag == 0 { //首次错误
					timestamp = cast.ToString(time.Now().Unix()*10 + 11) //多加1秒
					valueStr, _ := err.Msg.Value.Encode()
					logger.Ex(ctx, tag+".ProducerOutPut", fmt.Sprintf("Kafka Send Message Error:'%v',Topic:'%s',Data:'%v'", err.Err, err.Msg.Topic, string(valueStr)))
				}
				if diff >= 59 { //
					timestamp = cast.ToString(time.Now().Unix()*10 + 11) //多加一秒，防止同一秒可能出现打多次
					valueStr, _ := err.Msg.Value.Encode()
					logger.Ex(ctx, tag+".ProducerOutPut", fmt.Sprintf("Kafka Send Message Error:'%v',Topic:'%s',Data:'%v'", err.Err, err.Msg.Topic, string(valueStr)))
				}
				k.failMode.Do(k.fallback, []byte(timestamp+" "+meta.Kafka+" "+err.Msg.Topic+" "+string(loggerid)+" "+string(data)), data, []interface{}{meta.Kafka, err.Msg.Topic})
				byteMsg := err.Msg.Metadata.([]byte)
				byteMsg = byteMsg[:0]
				internal.BytePool.Put(byteMsg)
			}
		}
	}
}

func (k *Kafka) run(quite chan struct{}) {
	defer meta.Recovery()

	ctx := context.Background()
	tag := "kafka.run"

	t := time.NewTicker(time.Second * 60)
	for {
		select {
		case <-t.C:
			succ := atomic.SwapInt64(&k.successCount, 0)
			fail := atomic.SwapInt64(&k.errorCount, 0)
			logger.Ix(ctx, tag+".KafkaProducerOutPut", fmt.Sprintf("Stat succ:%d,fail:%d", succ, fail))
		case msg := <-k.input:
			items := bytes.SplitN(msg, []byte(" "), 5)
			topic := string(items[2])
			loggerid := items[3]
			data := items[4]
			k.producer.Input() <- &sarama.ProducerMessage{
				Metadata: msg,
				Topic:    topic,
				Key:      sarama.ByteEncoder(loggerid),
				Value:    sarama.ByteEncoder(data),
			}
		case <-k.exit:
			t.Stop()
			logger.Ix(ctx, tag+".ProducerOutPut", "KafkaQuit begin")
			if errors, ok := k.producer.Close().(sarama.ProducerErrors); ok {
				for _, err := range errors {
					items := bytes.SplitN(err.Msg.Metadata.([]byte), []byte(" "), 5)
					timestamp := string(items[0])
					loggerid, _ := err.Msg.Key.Encode()
					data, _ := err.Msg.Value.Encode()
					k.fallback <- []byte(timestamp + " " + meta.Kafka + " " + err.Msg.Topic + " " + string(loggerid) + " " + string(data))
				}
				logger.Ix(ctx, tag+".ProducerOutPut", fmt.Sprintf("KafkaQuit recovered:%d", len(errors)))
			}
			logger.Ix(ctx, tag+".ProducerOutPut", "KafkaQuit end")
			close(k.watchExit)
			close(k.successExit)
			close(k.fallExit)
			close(quite)
			return
		}
	}
}
