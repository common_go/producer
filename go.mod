module gitcode.com/common_go/producer

go 1.15

require (
	gitcode.com/common_go/config v0.1.0
	gitcode.com/common_go/logger v0.1.0
	github.com/Shopify/sarama v1.29.1
	github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/go-redis/redis/v8 v8.10.0
	github.com/spf13/cast v1.3.1
	github.com/tidwall/gjson v1.8.0
	github.com/tidwall/sjson v1.1.7
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0 // indirect
)
