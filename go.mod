module gitee.com/common_go/producer

go 1.15

require (
	gitee.com/common_go/config v0.0.1
	gitee.com/common_go/logger v0.0.1
	github.com/Shopify/sarama v1.29.1
	github.com/confluentinc/confluent-kafka-go v1.7.0
	github.com/go-redis/redis/v8 v8.10.0
	github.com/spf13/cast v1.5.0
	github.com/tidwall/gjson v1.8.0
	github.com/tidwall/sjson v1.1.7
)
