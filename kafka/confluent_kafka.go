// +----------------------------------------------------------------------
// |
// |
// |
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/8/10 4:30 下午
// +----------------------------------------------------------------------
package kafka

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"sync/atomic"
	"time"

	"gitcode.com/common_go/config"
	"gitcode.com/common_go/logger"
	"gitcode.com/common_go/producer/common"
	"gitcode.com/common_go/producer/failmode"
	"gitcode.com/common_go/producer/internal"
	"gitcode.com/common_go/producer/meta"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/cast"
)

type CKafka struct {
	input    chan []byte
	fallback chan<- []byte

	successCount int64
	errorCount   int64

	cfg      *kafka.ConfigMap
	producer *kafka.Producer
	failMode failmode.FailMode

	exit chan struct{}
}

func CInit(quit chan struct{}, fallBack chan<- []byte) common.MQ {
	cfg := &kafka.ConfigMap{
		"api.version.request": "true",
		"message.max.bytes":   1000000,
		"linger.ms":           10,
		"retries":             30,
		"retry.backoff.ms":    1000,
		"acks":                "1"}

	cfg.SetKey("bootstrap.servers", strings.Join(config.GetConfArr("KafkaProxy", "brokers"), ","))

	if config.GetConfDefault("KafkaProxy", "sasl", "false") != "true" {
		cfg.SetKey("security.protocol", "plaintext")
	} else {
		cfg.SetKey("sasl.mechanism", "PLAIN")
		cfg.SetKey("security.protocol", "sasl_plaintext")
		cfg.SetKey("sasl.username", config.GetConf("KafkaProxy", "user"))
		cfg.SetKey("sasl.password", config.GetConf("KafkaProxy", "password"))
		//cfg.SetKey("sasl.mechanism", config.GetConf("KafkaProxy", "mechanism"))
	}

	k := new(CKafka)
	k.cfg = cfg
	k.input = make(chan []byte, 0)
	k.fallback = fallBack
	k.exit = make(chan struct{}, 1)
	k.successCount = int64(0)
	k.errorCount = int64(0)

	k.initProducer()
	go k.event()
	go k.run(quit)

	return k
}

func (k *CKafka) Input() chan<- []byte {
	return k.input
}

func (k *CKafka) Close() {
	close(k.exit)
}

func (k *CKafka) SetFailMode() {
	var err error
	k.failMode, err = failmode.GetFailMode(meta.Kafka, config.GetConfDefault("KafkaProxy", "failMode", "discard"))
	if err != nil {
		logger.Wx(context.Background(), "SetFailMode", "Kafka SetFailMode error", "error", err.Error())
	}
}

func (k *CKafka) initProducer() {
	producer, err := kafka.NewProducer(k.cfg)
	if err != nil {
		logger.Wx(context.Background(), "initProducer", "ProducerInit", "error", err.Error())
	}

	k.producer = producer
}

func (k *CKafka) event() {
	defer meta.Recovery()

	ctx := context.Background()
	tag := "kafka.success"

	for e := range k.producer.Events() {
		switch ev := e.(type) {
		case *kafka.Message:
			if ev.TopicPartition.Error != nil {
				logger.Wx(ctx, tag, fmt.Sprintf("delivery failed: %v", ev.TopicPartition))

				atomic.AddInt64(&k.errorCount, int64(1))
				items := bytes.SplitN([]byte(*ev.TopicPartition.Metadata), []byte(" "), 5)
				timestamp := string(items[0])
				loggerId := ev.Key
				data := ev.Value
				logFlag := cast.ToInt64(timestamp) % 10
				diff := time.Now().Unix() - cast.ToInt64(timestamp)/10
				if logFlag == 0 { //首次错误
					timestamp = cast.ToString(time.Now().Unix()*10 + 11) //多加1秒
					valueStr := data
					logger.Ex(ctx, tag+".ProducerOutPut", fmt.Sprintf("Kafka Send Message Error:'%v',Topic:'%s',Data:'%v'", ev.TopicPartition.Error, ev.TopicPartition.Topic, string(valueStr)))
				}
				if diff >= 59 { //
					timestamp = cast.ToString(time.Now().Unix()*10 + 11) //多加一秒，防止同一秒可能出现打多次
					valueStr := data
					logger.Ex(ctx, tag+".ProducerOutPut", fmt.Sprintf("Kafka Send Message Error:'%v',Topic:'%s',Data:'%v'", ev.TopicPartition.Error, ev.TopicPartition.Topic, string(valueStr)))
				}
				k.failMode.Do(k.fallback, []byte(timestamp+" "+meta.Kafka+" "+*ev.TopicPartition.Topic+" "+string(loggerId)+" "+string(data)), data, []interface{}{meta.Kafka, ev.TopicPartition.Error})
				byteMsg := []byte(*ev.TopicPartition.Metadata)
				byteMsg = byteMsg[:0]
				internal.BytePool.Put(byteMsg)

			} else {
				logger.Dx(ctx, tag+".ProducerOutPut", fmt.Sprintf("kafka send message success. to %v", ev.TopicPartition))
			}
		}
	}
}

func (k *CKafka) run(quite chan struct{}) {
	defer meta.Recovery()

	ctx := context.Background()
	tag := "kafka.run"

	t := time.NewTicker(time.Second * 60)
	for {
		select {
		case <-t.C:
			succ := atomic.SwapInt64(&k.successCount, 0)
			fail := atomic.SwapInt64(&k.errorCount, 0)
			logger.Ix(ctx, tag+".KafkaProducerOutPut", fmt.Sprintf("Stat succ:%d,fail:%d", succ, fail))
		case msg := <-k.input:
			items := bytes.SplitN(msg, []byte(" "), 5)
			timestamp := string(items[0])
			topic := string(items[2])
			loggerId := items[3]
			data := items[4]
			metadata := string(msg)

			err := k.producer.Produce(&kafka.Message{
				TopicPartition: kafka.TopicPartition{
					Topic:     &topic,
					Partition: kafka.PartitionAny,
					Metadata:  &metadata,
				},
				Value: data,
				Key:   loggerId,
			}, nil)

			if err != nil {
				k.fallback <- []byte(timestamp + " " + meta.Kafka + " " + topic + " " + string(loggerId) + " " + string(data))
			}

		case <-k.exit:
			t.Stop()
			logger.Ix(ctx, tag+".ProducerOutPut", "KafkaQuit begin")
			k.producer.Flush(15 * 1000)
			k.producer.Close()
			logger.Ix(ctx, tag+".ProducerOutPut", "KafkaQuit end")
			close(quite)
			return
		}
	}
}
